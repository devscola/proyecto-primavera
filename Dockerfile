FROM node:lts-alpine

RUN mkdir /app

COPY package.json package-lock.json /app/

WORKDIR /app

RUN npm install

COPY . .

CMD ["npm", "run", "start"]
