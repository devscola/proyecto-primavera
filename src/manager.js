const managerTemplate = document.createElement('template')
managerTemplate.innerHTML = `<auto-evaluation>
  </auto-evaluation>`

export default class Manager extends HTMLElement {
    constructor(){
        super()
        this.shadowDOM = this.attachShadow({ mode:'open'})
        this.shadowDOM.appendChild(managerTemplate.content.cloneNode(true))
    }

}
customElements.define('manager-auto',Manager)