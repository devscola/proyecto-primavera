const resourceTemplate = document.createElement('template')
resourceTemplate.innerHTML =`
<style>
div {
    border-radius:10px;
    margin:10px;

}

div > li {
    list-style:none;
    margin-left:10px;
}

a{
    text-decoration:none
}


    
</style>
<div>
    <li>
        <a></a>
    </li>
</div>
`

export default class Resource extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode:'open'})
        this.shadowDOM.appendChild(resourceTemplate.content.cloneNode(true))
    }
    attributeChangedCallback(attr, oldAttr, newAttr) {
        switch (attr) {
            case "name":
                this.shadowDOM.querySelector('a').innerText = newAttr
                break;
            case "link":
                this.shadowDOM.querySelector('a').href = newAttr
                break
        }

    }
    static get observedAttributes() {
        return ['name', 'link']
    }
}
window.customElements.define('resource-list', Resource)

