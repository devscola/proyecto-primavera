import KeyKnowledge from '../src/components/KeyKnowledge'

test('writes the first line of the key knowledge list', () => {
    const knowledge = '¿Qué es la terminal?'
    document.body.innerHTML = `
    <key-knowledge 
        knowledge='¿Qué es la terminal?' 
        content='Se define como terminal o consola, a todo dispositivo electrónico que
        forma parte del Hardware de un ordenador, y que tiene la funcionalidad básica
        de ingresar o mostrar los datos que se encuentran dentro de una computadora o en
        un determinado sistema de computación.'>
    </key-knowledge>`
    expect(document.body.innerHTML).toContain(knowledge)
})