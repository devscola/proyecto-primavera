const titleTemplate = document.createElement('template')
titleTemplate.innerHTML = `
    <style>
        h1{
            text-align:center;
            text-transform:uppercase;
            margin-top: 50px
        }
    </style>
    <h1>Command Line</h1>
    <h2>Objetivo:</h2>
    <h3>Aprender los comandos básicos para desenvolverse con la línea de comandos.</h3>
    `

export default class Title extends HTMLElement {
    constructor(){
        super()
        this.shadowDOM = this.attachShadow({ mode:'open'})
        this.shadowDOM.appendChild(titleTemplate.content.cloneNode(true))
    }
}
customElements.define('command-title',Title)















