## [Proyecto Primavera](https://devscola.gitlab.io/proyecto-primavera/)

## Table of Contents
* [Installation](#Installation)
    * [Node](#Node)
    * [Docker](#Docker)
* [Running Docker](#Running Docker)
* [Running Tests](#Running Tests)


## Installation
To be able to run the code on the docker you must have Node, Docker and Docker-compose. We will tell you how to install them on the following instructions:

### Node

1. Download Node from the official webpage: [Node Js' official page](https://nodejs.org/en/download/)

2. Open a new terminal and type the following commands:
```{
    $ cd /opt change to opt directory
    # sudo mv ~/downloads/node . (Note the . at the end of the command)
    # sudo tar -xf node-v12.16.3.tar.gz
    # sudo node-v12.16.3.tar.gz/bin
    $ pwd 

``` 

3. Copy the route
4. Continue typing the following commands on the same terminal:
```{
    # sudo ln -s [paste pwd route]/node  /usr/local/bin/node
    # sudo ln -s [paste pwd route]/npm  /usr/local/bin/npm
    $ cd ~
```

### Docker

1. To install Docker execute these commands
```{
    #sudo apt get update

    #sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    #sudo apt-key fingerprint 0EBFCD88

    #sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

    #sudo apt-get install docker-ce docker-ce-cli containerd.io

    #sudo apt get update
```
2. Test Docker
```
    #sudo docker run hello-world
```

3. Build docker
```
    #sudo docker build -t [IMAGE NAME]
```

4. Run docker
```
    #sudo docker run -it --rm -p 3000:3000 [IMAGE NAME]
```

### Docker-compose

1. Build docker-compose 
```
  - sudo docker-compose up --build -d
```

2. Run docker-compose 
```
  - sudo docker-compose up
```
hola!! mundo