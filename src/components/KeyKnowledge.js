const knowledgeTemplate = document.createElement('template')
knowledgeTemplate.innerHTML = `
<style>
    p{
        margin-left:25px;
        margin-bottom:35px
    }
</style>

<div>
<h3></h3>
<p></p>
</div>`

export default class Knowledge extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode:'open'})
        this.shadowDOM.appendChild(knowledgeTemplate.content.cloneNode(true))
        }

    attributeChangedCallback(attr, oldAttr, newAttr) {
        switch (attr) {
            case "knowledge":
                this.shadowDOM.querySelector('h3').innerHTML = newAttr
                break;
            case "content":
                this.shadowDOM.querySelector('p').innerHTML = newAttr
                break;
            }
    }
    static get observedAttributes() {
        return ['knowledge', 'content']
    }
}
customElements.define('key-knowledge', Knowledge)
