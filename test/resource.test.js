import Resource from '../src/components/Resource'

test('writes the first line of the resources list', () => {
    const resource = 'Command Line Challenge'
    document.body.innerHTML = `<resource-list resource ='Command Line Challenge'></resource-list>`
    expect(document.body.innerHTML).toContain(resource)
})
test('writes the second line of the resources list', () => {
    const resource = 'Learning the shell'
    document.body.innerHTML = `<resource-list resource='Learning the shell'></resource-list>`
    expect(document.body.innerHTML).toContain(resource)
})
test('writes the thrid line of the resources list', () => {
    const resource = 'Writting Shell Scripts'
    document.body.innerHTML = `<resource-list resource='Writting Shell Scripts'></resource-list>`
    expect(document.body.innerHTML).toContain(resource)
})
test('writes the fourth line of the resources list', () => {
    const resource = 'Command Line Mistery'
    document.body.innerHTML = `<resource-list resource='Command Line Mistery'></resource-list>`
    expect(document.body.innerHTML).toContain(resource)
})
test('writes the fifth line of the resources list', () => {
    const resource = 'Linea de Comandos en Castellano'
    document.body.innerHTML = `<resource-list resource='Linea de Comandos en Castellano'></resource-list>`
    expect(document.body.innerHTML).toContain(resource)
})
test('writes the sixth line of the resources list', () => {
    const resource = 'KataCoda: Linux Commands'
    document.body.innerHTML = `<resource-list resource='KataCoda: Linux Commands'></resource-list>`
    expect(document.body.innerHTML).toContain(resource)
})