const autoevaluationTemplate = document.createElement('template')
autoevaluationTemplate.innerHTML = `
<style>
    h3{
        font-size:30px
    }

    div > input {
        margin-left: 20px;
        margin-bottom: 20px;
        text-align: center;
    }

</style>

<h3>Autoevaluación</h3>
<h4>
    Vaya haciendo tick en los recuadros cuando conozca y comprenda el comando.
</h4>
<div id='div'>
    <input id="cd" type = checkbox>cd</input>
    <input id="ls" type = checkbox>ls</input>
    <input id="pwd" type = checkbox>pwd</input>
    <input id="touch" type = checkbox>touch</input>
    <input id="grep" type = checkbox>grep</input>
    <input id="g" type = checkbox>g</input>
    <input id="rep" type = checkbox>rep</input>
    <input id="more" type = checkbox>more/less/cat</input>
    <input id="rm" type = checkbox>rm</input>
    <input id="rmdir" type = checkbox>rmdir</input>
    <input id="mkdir" type = checkbox>mkdir</input>
    <input id="tail" type = checkbox>tail</input>
    <input id="mv" type = checkbox>mv</input>
    <input id="smaller" type = checkbox><</input>
    <input id="bigger" type = checkbox>></input>
    <input id="echo" type = checkbox>echo</input>
    <input id="and" type = checkbox>|</input>
</div>
`

export default class Autoevaluation extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(autoevaluationTemplate.content.cloneNode(true))
        this.checkedElements = document.querySelectorAll('.checkbox:checked');
        this.checkboxes = this.shadowDOM.querySelectorAll('.checkbox');
        this.div = this.shadowDOM.querySelector('div')
        this.cd = this.shadowDOM.getElementById('cd')
        this.ls  = this.shadowDOM.getElementById('ls')
        this.pwd = this.shadowDOM.getElementById('pwd')
        this.touch = this.shadowDOM.getElementById('touch')
        this.grep = this.shadowDOM.getElementById('grep')
        this.g = this.shadowDOM.getElementById('g')
        this.rep = this.shadowDOM.getElementById('rep')
        this.more = this.shadowDOM.getElementById('more')
        this.rm = this.shadowDOM.getElementById('rm')
        this.rmdir = this.shadowDOM.getElementById('rmdir')
        this.mkdir = this.shadowDOM.getElementById('mkdir')
        this.tail = this.shadowDOM.getElementById('tail')
        this.mv = this.shadowDOM.getElementById('mv')
        this.tail = this.shadowDOM.getElementById('tail')
        this.smaller = this.shadowDOM.getElementById('smaller')
        this.bigger = this.shadowDOM.getElementById('bigger')
        this.echo = this.shadowDOM.getElementById('echo')
        this.and = this.shadowDOM.getElementById('and')
    }

    connectedCallback(){
        this.div.addEventListener('change', function() {
            if (this.checkedElements.lenght === 17) {
                console.log("Checkbox is checked..");
            } else {
                console.log("Checkbox is not checked..");
            }
        })
    }
}
window.customElements.define("auto-evaluation", Autoevaluation);