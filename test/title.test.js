import Title from '../src/components/Title'

test('writes the title: Command Line',()=>{
    const expectedTitle = 'Command Line'
    const componentTitle = document.createElement('command-title')
    document.body.appendChild(componentTitle)

    const title = componentTitle.shadowRoot.querySelector('h1')

    expect(title.innerHTML).toContain(expectedTitle)
})

