const basicTemplate = document.createElement('template')
basicTemplate.innerHTML = `

<style>
    div{
        margin-bottom:30px
    }

    li{
        margin-left:20px;
        list-style:none;
    }
    
    a{
        text-decoration:none
    }
</style>

<div>
<h3>Realice los siguientes ejercicios:</h3>
<li><a href=https://cmdchallenge.com/#/hello_world >Hello World</a></li>
<li><a href=https://cmdchallenge.com/#/current_working_directory >Current Working Directory</a></li>
<li><a href=https://cmdchallenge.com/#/list_files >List Files</a></li>
<li><a href=https://cmdchallenge.com/#/print_file_contents >Print File Contents</a></li>
<li><a href=https://cmdchallenge.com/#/last_lines >Last Lines</a></li>
<li><a href=https://cmdchallenge.com/#/create_file >Create File</a></li>
<li><a href=https://cmdchallenge.com/#/create_directory >Create Directory</a></li>
<li><a href=https://cmdchallenge.com/#/copy_file >Copy File</a></li>
<li><a href=https://cmdchallenge.com/#/move_file >Move File</a></li>
<li><a href=https://cmdchallenge.com/#/create_symlink >Create Symlink</a></li>
<li><a href=https://cmdchallenge.com/#/delete_files >Delete Files</a></li>
<li><a href=https://cmdchallenge.com/#/remove_files_with_extension >Remove Files With Expresions</a></li>
<li><a href=https://cmdchallenge.com/#/find_string_in_a_file >Find String In A File</a></li>
<li><a href=https://cmdchallenge.com/#/search_for_files_containing_string >Search For Files Containing String</a></li>
<li><a href=https://cmdchallenge.com/#/search_for_files_by_extension >Search For Files By Expresion</a></li>
<li><a href=https://cmdchallenge.com/#/search_for_string_in_files_recursive >Search For Strings In Files Recursive</a></li>

</div>
`

export default class Basic extends HTMLElement {
    constructor(){
        super()
        this.shadowDOM = this.attachShadow({ mode:'open'})
        this.shadowDOM.appendChild(basicTemplate.content.cloneNode(true))
    }
}
customElements.define('command-basic',Basic)
