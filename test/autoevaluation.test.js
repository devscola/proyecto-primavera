import Autoevaluación from '../src/components/Autoevaluation'

test('writes the title: Autoevaluación', () => {

    const expectedTitle = 'Autoevaluación'
    const componentTitle = document.createElement('auto-evaluation')
    document.body.appendChild(componentTitle)

    const title = componentTitle.shadowRoot.querySelector('h3')

    expect(title.innerHTML).toContain(expectedTitle)

})